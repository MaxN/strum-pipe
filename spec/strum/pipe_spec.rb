# frozen_string_literal: true

RSpec.describe Strum::Pipe do
  it "has a version number" do
    expect(Strum::Pipe::VERSION).not_to be nil
  end

  let(:first_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) {} # rubocop:disable Lint/EmptyBlock
    end
  end

  let(:second_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) {} # rubocop:disable Lint/EmptyBlock
    end
  end

  it "empty result" do
    expect(Strum::Pipe.call(first_service_class, second_service_class)).to eq({})
  end
end
