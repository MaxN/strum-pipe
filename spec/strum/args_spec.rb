# frozen_string_literal: true

RSpec.describe "Pipe unit with 'to:'" do
  let(:first_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) do
        output(input)
      end
    end
  end

  let(:second_service_class) do
    Class.new do
      include Strum::Service
      define_method(:call) do
        output(arg1)
      end
    end
  end

  # rubocop: disable Metrics/BlockLength
  [
    { name: "arg value is integer", value: 10 },
    { name: "arg value is hash", value: { demo: :demo } },
    { name: "arg value is string", value: "demo value" },
    { name: "arg value is boolean", value: true }
  ].each do |params|
    describe params[:name] do
      it "success without block" do
        expect(
          Strum::Pipe.call(
            [first_service_class, { to: :arg1 }],
            [second_service_class],
            input: params[:value]
          )
        ).to eq(params[:value])
      end

      it "success with block" do
        expect(
          Strum::Pipe.call(
            [first_service_class, { to: :arg1 }],
            [second_service_class],
            input: params[:value]
          ) do |m|
            m.success { |result| result }
            m.failure { |errors| errors }
          end
        ).to eq(params[:value])
      end

      it "wrong without block" do
        expect do
          Strum::Pipe.call(
            [first_service_class, { to: :arg2 }],
            [second_service_class],
            input: params[:value]
          )
        end.to raise_error(NameError).with_message(/undefined local variable or method `arg1'/)
      end

      it "wrong with block" do
        expect do
          Strum::Pipe.call(
            [first_service_class, { to: :arg2 }],
            [second_service_class],
            input: params[:value]
          ) do |m|
            m.success { |result| result }
            m.failure { |errors| errors }
          end
        end.to raise_error(NameError).with_message(/undefined local variable or method `arg1'/)
      end
    end
  end
  # rubocop: enable Metrics/BlockLength
end
